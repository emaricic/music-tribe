from .models import  Comment
from django import forms
from django.forms import ModelForm, fields
from bootstrap_datepicker_plus.widgets import DateTimePickerInput



class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ['text']
        button = ['approved']