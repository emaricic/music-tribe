
from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse,reverse_lazy

# Create your views here.


def index(request):
    return render(request, 'app/index.html')
